/**
 * 
 */

/*

LoadUploadImg now UploadImg as there is no distionction between the two anymore
Deals with showing a preview before a resized image is loaded in from the server
We no longer require the actual upload object to be defined here as we can determine the remote location to fetch the image without this information

NOTE: compared to how this is implemented elsewhere we use upload-img- as the css class prefix for this component rather than uploadImg => backwards compat issues

*/

var _pixelRatio;
if (Meteor.isClient) {
	_pixelRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
} else {
	_pixelRatio = 1;
}

UploadImg = React.createClass({

	/** Props/State **/

	propTypes: {
		upload_id: React.PropTypes.string, // or upload is required
		upload: React.PropTypes.object,
		page: React.PropTypes.number, // we use zero index here rather than an actual page number, ie index of page in the upload pages array

		width: React.PropTypes.number,
		height: React.PropTypes.number,
		
		mode: React.PropTypes.oneOf(['size', 'fit', 'fill', 'cover']), // fit => fit within the specified width and height
													  		 		   // size => use this size (if either are 0 => calculate one given the other)
													  		  		   // fill => fill this size, possibly by padding out in a div if necessary, might also clip a bit
													  		  		   // cover => fill this size by filling and clipping the image (ie the entire area will always be covered)

		noUploadImgClass: React.PropTypes.bool, // sometimes we wrap in this class
	},
	getDefaultProps: function () {
		return {
			mode: 'size',
			page: 0, // ie preview for whole doc
			// noUploadImgClass: false
		};
	},

	getInitialState: function () {
		return {
			loaded: false, // has the image been loaded ? (with a particular width, height, src)
			upload_id: this.props.upload ? this.props.upload._id : this.props.upload_id
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** Load Image **/

	refreshImage: function () {
		if (this.state.loaded !== this._imgSrc()) {
			this.setState({loaded: false});

			this.loadImage();
		}
	},
	loadImage: function () {
		var image = new Image();
		image.onload = function () { this._didLoadImage(image); }.bind(this);
		image.src = this._imgSrc();
	},
	_didLoadImage: function (image) {
		if (!this.isMounted()) return;
		if (image.src != this._imgSrc()) return;

		this.setState({
			loaded: image.src,

			width: image.width/_pixelRatio, // _pixelRatio is just an internal adjustment we do behind the scenes, for all intents and purposes it can be factored out for our calculations
			height: image.height/_pixelRatio
		});
	},
	_imgSrc: function () {
		var width = this.props.width;
		var height = this.props.height;

		var _mode;
		if (this.props.mode === 'size') {
			_mode = 'fill';

			// we transform our width and height to a width with a multiple of 1000 (or 500s) and apply a relative transformation to the height we load in
			var origWidth = width;
			width = this._roundWidth(width);
			height = Math.ceil(height * (width/origWidth));
		} else if (this.props.mode === 'cover') {
			_mode = 'fill';

			width = this._roundWidth(width);
			height = 0; // we grab a height for the width and then hope we can cover the height
		} else {
			_mode = 'fit';
		}

		var _global = typeof window === 'undefined' ? global : window;
		return _global.Uploads.staticUrlForSize(this.state.upload_id, this.props.page, width*_pixelRatio, height*_pixelRatio, _mode);
	},
	_roundWidth: function (width) {  // we pick a rounded widths when getting _imgSrc so that we don't generate a ridiculous number of thumbs
		if (width < 100) {
			return 100;
		} else if (width < 250) {
			return 250;
		} else if (width < 500) {
			return 500;
		} else {
			return Math.ceil(width/1000)*1000;
		}
	},

	/** Sizing Loaded Image **/
	// We need these utils because the size of the image loaded in does not necessarily have to reflect the props directly - we could load in a certain cached size for example rather than generate the exact size each time it is requested

	heightForWidth: function (width) {
		var ratio = width / this.state.width;
		return Math.round(this.state.height*ratio);
	},

	widthForHeight: function (height) {
		var ratio = height / this.state.height;
		return Math.round(this.state.width*ratio);
	},

	sizeToFitBounds: function (_width, _height) {
		if (this.state.width > this.state.height) {
			var resizedHeight = this.heightForWidth(_width);

			if (resizedHeight > _height) {
				return {width: this.widthForHeight(_height), height: _height};
			} else {
				return {width: this.widthForHeight(resizedHeight), height: resizedHeight};
			}
		} else {
			var resizedWidth = this.widthForHeight(_height);

			if (resizedWidth > _width) {
				return {width: _width, height: this.heightForWidth(_width)};
			} else {
				return {width: resizedWidth, height: this.heightForWidth(resizedWidth)};
			}
		}
	},

	/** React **/

	componentWillReceiveProps: function(nextProps) {
		this.setState({
			upload_id: nextProps.upload ? nextProps.upload._id : nextProps.upload_id
		});
	},
	componentDidUpdate: function(prevProps, prevState) {
		if (!_.isEqual(prevProps, this.props)) {
			this.refreshImage();
		}
	},
	componentDidMount: function() {
		this.refreshImage();
	},

	/** Render **/

	render: function () {
		var width = this.props.width;
		var height = this.props.height;

		if (!this.state.loaded) {
			// => show a temp gray block until loaded
			// we cannot show a preview because we don't even know the dimensions of the preview
			// TODO: if height or width is zero then give this block height and width at an approximate size which can change once the image has actually been loaded in

			var style = {
				'background': '#ccc', 
				'width': width, 
				'height': height
			}

			if (this.props.style) _.extend(style, this.props.style);

			var className = 'upload-img upload-img-loading' + (this.props.className ? ' ' + this.props.className : '');

			return (<div style={style} className={className} />);
		}

		/** Get Sizing **/

		if (this.props.mode === 'size') {
			
			// => use the props which were provided (we assume one of either width or height were provided)
			// if either were 0 => need to calculate them given the information we now have from loading in the image

			if (!width) width = this.widthForHeight(height);
			if (!height) height = this.heightForWidth(width);

		} else if (this.props.mode === 'fit' || this.props.mode === 'fill') {
			var size = this.sizeToFitBounds(width, height);

			width = size.width;
			height = size.height;
		}

		if (this.props.mode === 'cover') {

			var style = {
				'width': width, 
				'height': height, 
				'backgroundImage': 'url('+this._imgSrc()+')'
			}

			if (this.props.style) _.extend(style, this.props.style);

			return (<div className={"upload-img upload-img-coverContainer" + (this.props.className ? " "+this.props.className : "")} style={style} />);

		} else {

			/** Construct Upload Img **/

			var className = (this.props.noUploadImgClass ? '' : 'upload-img') + (this.props.className ? ' ' + this.props.className : '');
			var uploadImg = (<img src={this._imgSrc()} width={width} height={height} className={className} style={this.props.style} />);

			/** Return **/

			if (this.props.mode === 'fill') {
				var posLeft = Math.round((this.props.width - width) / 2.0);
				var posTop = Math.round((this.props.height - height) / 2.0);

				uploadImg = React.cloneElement(uploadImg, {
					style: {
						marginTop: posTop+'px',
						marginLeft: posLeft+'px',
					},
					noUploadImgClass: true,
					className: null
				})

				var thisClass = "upload-img upload-img-fillContainer" + (this.props.className ? " "+this.props.className : "");

				var thisStyle = {width: this.props.width+'px', height: this.props.height+'px'}
				if (this.props.style) _.extend(thisStyle, this.props.style);

				return (
					<div className={thisClass} style={thisStyle}>
						{uploadImg}
					</div>
				);
			} else {
				return uploadImg;
			}

		}
		
	},

});
