/** 

Uploads
Client side functionality

**/

// upload:
// Util function for uploading files
// Add it to uploads collection for sake of a neatness
// files is an array of files from a form file input
// callback is in form (err, result) where result.uploads is an array of uploads

_.extend(UploadCollection_Prototype, {

	upload: function (files, callback) {
		var _data = new FormData();
		_.each(files, function (value, key) {
			_data.append(key, value);
		});

		jQuery.ajax({
			url: HCConfig.uploads_server_url+'Uploads/upload',
			type: 'POST',
			data: _data,
			cache: false, 
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data) { // data, textStatus, jqXHR
				if (callback) callback(null, data);
			},
			error: function () {
				if (callback) callback(true); // todo: better error handling
			},
		});
	}

});