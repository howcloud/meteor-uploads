Meteor.startup(function () {

	window.Uploads = Collections.init("uploads", {
		transform: Upload_Transform
	});

	window.Uploads.extendCollection(UploadCollection_Prototype);
	window.Uploads.extendDocument(UploadDocument_Prototype);

});