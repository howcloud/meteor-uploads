Meteor.startup(function () {

	// We have to defer until startup bceause 

	var _uploadsCollection = null;
	if (HCConfig.uploadsDatabase) {
		var uploadsDatabase = new MongoInternals.RemoteCollectionDriver(HCConfig.uploadsDatabase);
		_uploadsCollection = new Mongo.Collection("uploads", { _driver: uploadsDatabase });
	}

	global.Uploads = Collections.init("uploads", {
		transform: Upload_Transform,
		collection: _uploadsCollection
	});

	global.Uploads.extendCollection(UploadCollection_Prototype);
	global.Uploads.extendDocument(UploadDocument_Prototype);

});