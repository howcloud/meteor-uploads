/** Uploads Collection **/

_.extend(UploadCollection_Prototype, {

	/** Image Generation **/
	// We define a helper function at the collection level because we don't necessarily neeed to load the entire upload representation to actually load an image based off of an upload

	staticUrlForSize: function (_id, page, width, height, mode) {
		// TODO: Decide here if we want to load a default size vs the actual size specified
		// We can have an alternative actualStaticUrlForSize which always guarantees an image exactly as specified vs the higher cache hit rate version returned here

		mode = mode || 'fill';
		page = page || 0;

		var _mode;
		if (mode === 'fill') {
			_mode = 'f';
		} else if (mode === 'fit') {
			_mode = 'm';
		}

		var url = HCConfig.uploads_static_cdn_url + 'uploads/i' + _id;

		url += '-p'+page;
		url += '-m' + _mode;

		if (width) url += '-w' + width;
		if (height) url += '-h' + height;

		return url + '.png';
	},

	downloadUrl: function (_id) {
		return HCConfig.uploads_server_url+'Uploads/view.php?a=d&i='+_id;
	},

})

/** Upload Document Prototype **/

_.extend(UploadDocument_Prototype, {

	/** Main Page **/

	preview: function () {
		if (!this.pages) return;

		return this.pages[0].preview();
	},

	/* URLs */

	urlToFitBounds: function (width, height) {
		if (!this.pages) return;

		return this.pages[0].urlToFitBounds(width, height);
	},

	urlForSize: function (width, height) {
		return this.pages[0].urlForSize(width, height);
	},

	urlForDownload: function () {
		return Uploads.downloadUrl(this._id);
	},
	urlForPreview: function () {
		return this.preview();
	},

	/* Sizing */

	heightForWidth: function (width) {
		return this.pages[0].heightForWidth(width);
	},

	widthForHeight: function (height) {
		return this.pages[0].widthForHeight(height);
	},

	sizeToFitBounds: function (width, height) {
		return this.pages[0].sizeToFitBounds(width, height);
	},

});

/** Upload Page Prototype **/

_.extend(UploadPage_Prototype, {

	/** URLs **/

	preview: function () {
		return this.urlToFitBounds(HCConfig.uploads_previewWidth, HCConfig.uploads_previewHeight);
	},

	urlToFitBounds: function (width, height) {
		var size = this.sizeToFitBounds(width, height);
		return this.urlForSize(size.width, size.height);
	},

	urlForSize: function (width, height) {
		return this.staticUrlForSize(width, height, 'fill');
	},

	staticUrlForSize: function (width, height, mode) {
		return Uploads.staticUrlForSize(this.upload_id, this.page, width, height, mode);
	},

	/** Sizing **/

	heightForWidth: function (width) {
		var ratio = width / this.width;
		return Math.round(this.height*ratio);
	},

	widthForHeight: function (height) {
		var ratio = height / this.height;
		return Math.round(this.width*ratio);
	},

	sizeToFitBounds: function (_width, _height) {
		if (this.width > this.height) {
			var resizedHeight = this.heightForWidth(_width);

			if (resizedHeight > _height) {
				return {width: this.widthForHeight(_height), height: _height};
			} else {
				return {width: this.widthForHeight(resizedHeight), height: resizedHeight};
			}
		} else {
			var resizedWidth = this.widthForHeight(_height);

			if (resizedWidth > _width) {
				return {width: _width, height: this.heightForWidth(_width)};
			} else {
				return {width: resizedWidth, height: this.heightForWidth(resizedWidth)};
			}
		}
	}

});

/** Document Data Loaders **/

// We separate these out into their own functions rather than as part of the object as there is no guarantee that by the time these return the object will exist (ie a new one could have loaded and been transformed)
// => Better to just have these as separate functions with callbacks
// We don't do any event listening on the document level
// Actually maybe this isn't quite right - as long as we have callbacks it's not a big deal.... hmmm...
// How do we know that the callback comes from an actual document that we are interested in and not an 'old version'....

// TODO

var Upload_loadDocumentData = function (onLoad) {

}

var UploadPage_loadDocumentData = function (onLoad) {

}

/** Image Loaders **/

// TODO