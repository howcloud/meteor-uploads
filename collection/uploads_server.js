/**

Uploads
Server side collection functionality

**/

/** Collection Methods **/

/** Collection Permissions **/

/** Publications **/

Meteor.publish("Upload_byId", function (_upload_id) {
	return Uploads.collection.find(_upload_id);
});

/** Collection Server Side Methods **/

Meteor.methods({
	// These are also exposed as methods rather than publications as we don't always need to make them reactive (eg when calling pushing through to the HowCloudClassroom)
	
	upload_getById: function (_upload_id) {
		var upload = Uploads.getById(_upload_id);
		if (!upload) throw new Meteor.Error(404, "Upload not found");

		return upload;
	},
})