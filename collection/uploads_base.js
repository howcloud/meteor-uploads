// Upload collection/document prototypes
// Here because we have to defer creating the collection until startup due to HCConfig not being loaded in and needed in uploads_init_server.js

UploadDocument_Prototype = {}
UploadCollection_Prototype = {}

// UploadPage_Prototype
// a result of needing to transform individual pages (ideally we'd move away from this but lots of old code uses it so...)

UploadPage_Prototype = {}

// Custom transformation script
// used to transform pages within an upload to use our prototype, too

Upload_Transform = function (upload) {
	var pages = upload.pages;
	upload.pages = []

	_.each(pages, function (pageData) {
		var page = Object.create(UploadPage_Prototype);
		page.upload_id = upload._id;
		_.extend(page, pageData);

		upload.pages.push(page);
	});

	return upload;
}