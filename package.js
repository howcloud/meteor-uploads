// NOTE: assumes a global HCConfig is available which overrides certain behaviours (ie which database we are connecting to on the server etc)

Package.describe({
	name: 'howcloud:uploads',
	version: '0.1.0',
	summary: 'Provides a way to connect to a central file upload processing system across HowCloud meteor apps (todo: abstract this for other applications)',
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('ddp');
	api.use('mongo');
	api.use('meteor');
	api.use('livedata');
	api.use('jquery');

	api.use('meteorhacks:async');
	
	api.use('howcloud:collection-base');
	api.use('howcloud:react');

	// api.use('howcloud:ui-base'); // possibly: need to check this

	/* Add files */

	// Collection

	api.add_files('collection/uploads_base.js', ['client', 'server']);
	api.add_files('collection/uploads_init_client.js', ['client']);
	api.add_files('collection/uploads_init_server.js', ['server']);
	api.add_files('collection/uploads_shared.js', ['client', 'server']);
	api.add_files('collection/uploads_client.js', ['client']);
	api.add_files('collection/uploads_server.js', ['server']);

	// Components

	api.add_files('components/UploadImg.jsx', ['client','server']);
	api.add_files('css/upload-img.css', ['client']);

	// TODO: add in UploadButton and UploadInput but they are dependent on meteor-ui-base and we don't want to introduce that dependency here necessarily

	/* Export */

	api.export('Uploads', ['client', 'server']);
	api.export('UploadImg', ['client', 'server']);

});